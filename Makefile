#####################################################################
#### Please don't change this file. Use component.mk instead ####
#####################################################################

# Oops, changed!
ESP_HOME=/opt/esp-open-sdk
SDK_BASE=/opt/esp-open-sdk/ESP8266_NONOS_SDK_V2.0.0_16_08_10
SMING_HOME=$(shell pwd)/Sming/Sming

ifndef SMING_HOME
$(error SMING_HOME is not set: please configure it as an environment variable)
endif

include $(SMING_HOME)/project.mk
