#include <SmingCore.h>
#include <CounterStream.h>
#include <Platform/Timers.h>
#include "suites.h"

#ifdef ENABLE_MALLOC_COUNT
#include <malloc_count.h>
#endif

/*
 * Based on Sming Basic_Ssl sample.
*/


int onDownload(HttpConnection& connection, bool success);


Timer procTimer;
HttpClient downloadClient;
OneShotFastMs connectTimer;
int start_ssl_heap_allocation;
int end_ssl_heap_allocation;
int request_index;
bool request_done;

void printHeap()
{
	Serial.println(_F("Heap statistics"));
	Serial.print(_F("  Free bytes:  "));
	Serial.println(system_get_free_heap_size());
#ifdef ENABLE_MALLOC_COUNT
	Serial.print(_F("  Used:        "));
	Serial.println(MallocCount::getCurrent());
	Serial.print(_F("  Peak used:   "));
	Serial.println(MallocCount::getPeak());
	Serial.print(_F("  Allocations: "));
	Serial.println(MallocCount::getAllocCount());
	Serial.print(_F("  Total used:  "));
	Serial.println(MallocCount::getTotal());
#endif
}

void grcSslInit(Ssl::Session& session, HttpRequest& request)
{
	debug_i("Initialising SSL session for GRC");
	static const Ssl::Fingerprint::Cert::Sha1 sha1FingerprintLuko PROGMEM = {
		0x47, 0x19, 0xF4, 0xFB, 0x5C, 0x49, 0x6C, 0xF2, 0x21, 0xD5,
		0x66, 0x39, 0xB2, 0xEB, 0x1E, 0xDB, 0xCD, 0x09, 0x19, 0x1A,
	};
	static const Ssl::Fingerprint::Cert::Sha1 sha1FingerprintLocalRsa PROGMEM = {
		0x79, 0xCA, 0xEF, 0xB4, 0x83, 0xC3, 0x12, 0x9B, 0x29, 0x0E,
		0x84, 0xB5, 0xBA, 0x51, 0x8B, 0xB9, 0x8C, 0xA3, 0x17, 0x7F,
	};
	static const Ssl::Fingerprint::Cert::Sha1 sha1FingerprintLocalEcc256 PROGMEM = {
		0x12, 0x12, 0x24, 0xD3, 0x0B, 0xFB, 0xDE, 0xA9, 0x8F, 0xD1,
		0x95, 0x60, 0xEF, 0xB3, 0x23, 0xE9, 0x7C, 0xF7, 0xEA, 0x5E,
	};
	static const Ssl::Fingerprint::Cert::Sha1 sha1FingerprintLocalEcc224 PROGMEM = {
		0x09, 0x97, 0x71, 0x5B, 0x56, 0x8E, 0x7B, 0x13, 0x27, 0xBA,
		0xA9, 0xFE, 0xF3, 0x7E, 0xA9, 0x9A, 0x0F, 0xFA, 0xCB, 0xA7
	};
	session.validators.pin(sha1FingerprintLuko);
	session.validators.pin(sha1FingerprintLocalRsa);
	session.validators.pin(sha1FingerprintLocalEcc256);
	session.validators.pin(sha1FingerprintLocalEcc224);
	session.options.verifyLater = true;
	session.maxBufferSize = Ssl::MaxBufferSize::K2;
	// TODO: change the cipher suite to test!
	session.cipherSuites = &ecdhe_ecdsa_with_aes_256_gcm_sha384;
	// session.cipherSuites = &ecdhe_ecdsa_with_aes_256_cbc_sha384;
	// session.cipherSuites = &ecdhe_ecdsa_with_aes_256_cbc_sha;
	// session.cipherSuites = &ecdhe_ecdsa_with_aes_128_gcm_sha256;
	// session.cipherSuites = &ecdhe_ecdsa_with_aes_128_cbc_sha256;
	// session.cipherSuites = &ecdhe_ecdsa_with_aes_128_cbc_sha;
	// session.cipherSuites = &ecdhe_rsa_with_aes_128_cbc_sha256;
	// session.cipherSuites = &ecdhe_rsa_with_aes_256_cbc_sha384;
	// session.cipherSuites = &rsa_with_aes_256_cbc_sha;
	// session.cipherSuites = &rsa_with_aes_128_cbc_sha;
}


void start_request()
{
	connectTimer.start();
	Serial.println(_F("========== Measure heap alloc from here"));
	start_ssl_heap_allocation = 0;

	// TODO: Use desired server here!
	auto requestLuko = new HttpRequest(F("https://192.168.1.29:5006/"));
	printHeap();

	start_ssl_heap_allocation = MallocCount::getCurrent();
	request_done = false;
	requestLuko->onSslInit(grcSslInit);
	requestLuko->onRequestComplete(onDownload);
	requestLuko->setResponseStream(new CounterStream);
	downloadClient.send(requestLuko);
}

int onDownload(HttpConnection& connection, bool success)
{
	auto elapsed = connectTimer.elapsedTime();

	Serial.print(_F("Got response code: "));
	auto status = connection.getResponse()->code;
	Serial.print(status);
	Serial.print(" (");
	Serial.print(httpGetStatusText(status));
	Serial.print(_F("), success: "));
	Serial.print(success);

	auto stream = connection.getResponse()->stream;
	assert(stream != nullptr);

	Serial.print(_F(", received "));
	Serial.print(stream->available());
	Serial.println(_F(" bytes"));

	auto& headers = connection.getResponse()->headers;
	for(unsigned i = 0; i < headers.count(); ++i) {
		Serial.print(headers[i]);
	}

	auto ssl = connection.getSsl();
	if(ssl != nullptr) {
		ssl->printTo(Serial);
	}

	Serial.print(_F("Time to connect and download page: "));
	Serial.println(elapsed.toString());

	end_ssl_heap_allocation = MallocCount::getCurrent() - start_ssl_heap_allocation;
	Serial.print(_F("========== Heap to connect and download page: "));
	Serial.println(end_ssl_heap_allocation);
	return 0;
}

void gotIP(IpAddress ip, IpAddress netmask, IpAddress gateway)
{
	Serial.print(F("Connected. Got IP: "));
	Serial.println(ip);
	request_index = 0;
	start_request();
}

void connectFail(const String& ssid, MacAddress bssid, WifiDisconnectReason reason)
{
	Serial.println(F("Connection broken"));
}

void init()
{
	Serial.begin(SERIAL_BAUD_RATE);
	Serial.systemDebugOutput(true);
	Serial.println(F("Ready for SSL tests"));

#ifdef ENABLE_MALLOC_COUNT
	MallocCount::enableLogging(true);
#endif

	auto heapTimer = new Timer;
	start_ssl_heap_allocation = 0;
	end_ssl_heap_allocation = 0;
	printHeap();
	heapTimer->initializeMs<5000>(printHeap).start();

	// Setup the WIFI connection
	WifiStation.enable(true);
	WifiStation.config(WIFI_SSID, WIFI_PWD);

	WifiEvents.onStationGotIP(gotIP);
	WifiEvents.onStationDisconnect(connectFail);
}